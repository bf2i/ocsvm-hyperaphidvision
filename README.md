# OCSVM-HyperAphidVision

## Description
Aphids cause severe damage on agricultural crops resulting in significant economic losses and the use of pesticides at high frequency with low efficiency. Monitoring aphid infestations by regular field surveys is time-consuming and does not always provide an accurate spatiotemporal distribution of pests. Therefore, an automated non-destructive method to detect and evaluate aphid infestation would be beneficial for targeted treatments. In this work, we present a machine-learning model to identify and quantify aphids, localizing their spatial distribution over the leaves, using One-Class Support-Vector Machine and Laplacian of Gaussians blob detection. To train this model, we built a first large database of aphids' hyperspectral images, captured in a controlled laboratory environment. This database contains more than 160 images from three aphid lines distinctive in color, shape and developmental stage, and laying on leaves or neutral backgrounds. This system exhibits high quality validation scores, namely a Precision of 0.98, a Recall of 0.91, a F1  score equal to 0.94 and an AUPR score of 0.98. Moreover, the assessment of this method on new challenging images did not output False Negatives, and only few False Positives. Our results suggest that such a machine-learning model could be a promising tool to detect aphids for targeted treatments in the field


+ Single aphid on leave with complex background [Blob]
![single aphid blob](images/squares_one_aphid.png)

+ Single aphid on leave with complex background [Scores]
![single aphid score](images/score_one_aphid.png)

+ Aphid's group on leave [Blob]
![group aphids blob](images/squares_leave.png)

+ Aphid's group on leave [Scores]
![group aphids score](images/scores_leave.png)



+ `training_sensitivity_OC-SVM.ipynb` : Notebook to train the OC-SVM, exploring the best parameter setting
+ `visualize_SVM_boundaries_20211004.ipynb` : Notebook with visualization and model inspection
+ `aphid_detection_OC-SVM-LoG.ipynb` : Notebook with aphid detection based on OC-SVM and LoG

## Authors and acknowledgment
Sergio Peignier and Virginie Lacotte
